package com.fpeter.volley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class actividad extends AppCompatActivity {

    TextView textView1,textView2,textView3,textView4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad);

        String lista = (String) getIntent().getSerializableExtra("cedula");
    mostrar(lista);
    }
    public void mostrar (String id) {

        final String url2="http://168.62.50.94/appfacci/materia/"+id;

        final RequestQueue requestQueue = Volley.newRequestQueue(actividad.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url2,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        textView1=(TextView) findViewById(R.id.text1);

                        try{
                            JSONArray jsonArray =new JSONArray(response);

                            for (  int i=0;i<jsonArray.length();i++){

                                Log.e("asd",jsonArray.getJSONObject(i).getString("materia"));
                                textView1.setText(jsonArray.getJSONObject(i).getString("materia"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                requestQueue.stop();
            }
        });
        requestQueue.add(stringRequest);
    }
}
