package com.fpeter.volley;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    ImageView ivImageViewFromUrl;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter1;
    private ArrayList<String> arrayList1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=(ListView)findViewById(R.id.lista1);


        //ivImageViewFromUrl = (ImageView)findViewById(R.id.ima);
        //Picasso.with(getApplicationContext()).load(URL).into(ivImageViewFromUrl);


        mostrar();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String listChoice = (String) parent.getItemAtPosition(position);
                  Log.e("asd",listChoice);
                Intent intent = new Intent(MainActivity.this,
                        actividad.class);
                intent.putExtra("cedula", listChoice);
                startActivity(intent);

            }
        });

    }

    public void mostrar (View view) {

        final String url2="http://168.62.50.94/appfacci/materia/";

        final RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url2,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("sfs", response);
                        cargarlista(new String(response));

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                requestQueue.stop();
            }
        });
        requestQueue.add(stringRequest);
    }

    private  void cargarlista(String res){
        listView=(ListView)findViewById(R.id.lista1);
        arrayList1 =new ArrayList<String>();

        try{
            JSONArray jsonArray =new JSONArray(res);

            Log.e("fs", String.valueOf(jsonArray));
            for (  int i=0;i<jsonArray.length();i++){

                Log.e("asd",jsonArray.getJSONObject(i).getString("nivel"));
                arrayList1.add(jsonArray.getJSONObject(i).getString("nivel"));

            }
            arrayAdapter1=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,arrayList1);

            listView.setAdapter(arrayAdapter1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
